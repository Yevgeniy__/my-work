var btn = document.getElementsByClassName("showButton");
var tab = document.getElementsByClassName("tab");

btn[0].onclick = function(event) {
tab[0].classList.add("active");

tab[1].classList.remove("active");
tab[2].classList.remove("active");
          }

btn[1].onclick = function(event) {
tab[1].classList.add("active");

tab[0].classList.remove("active");
tab[2].classList.remove("active");
          }

btn[2].onclick = function(event) {
tab[2].classList.add("active");

tab[0].classList.remove("active");
tab[1].classList.remove("active");
          }
          
function hideAllTabs() {
  for (var i=0; i<tab.length; i++){
    tab[i].classList.remove("active"); 
    }  
   }    
   hideAllTabs(); 
       
 /*
    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
        + бонус выбрать одним селектором        

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
        и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
        Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */


  //1. Выбрать каждую кнопку в шапке
